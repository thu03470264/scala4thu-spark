import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac039 on 2017/5/8.
  */
object KeyVuleApp extends App{

  val conf = new SparkConf().setAppName("KeyVule")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)

  val kvRdd=sc.parallelize(1 to 100).map(v=>{
    if (v%2==0) "even"->v else "odd"->v
  })

  //kvRdd.mapValues(_+1).take(10).foreach(println)
  //kvRdd.groupByKey()
    //.mapValues(v=>v.sum)
    //.foreach(println)
  kvRdd.reduceByKey((acc,curr)=>acc+curr)
    .foreach(println)


}
