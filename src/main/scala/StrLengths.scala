import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac039 on 2017/5/1.
  */
object StrLengths extends App {
  val conf=new SparkConf().setAppName("StrLength").setMaster("local[*]")

  val sc=new SparkContext(conf)

  val nums=sc.textFile("nums.txt")

  val StrLength: RDD[Int] = nums.map(_.length)

  StrLength.take(10).foreach(println)
}
