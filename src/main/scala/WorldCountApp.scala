import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac039 on 2017/5/8.
  */
object WorldCountApp extends App{

  val conf = new SparkConf().setAppName("WorldCount")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)

  val lines=sc.textFile("/Users/mac039/Downloads/spark-doc.txt")
  val words=lines.flatMap(i=>i.split(" "))


//words.groupBy(str=>str).mapValues(_.size)
  words.map(word=>word->1).reduceByKey((acc,curr)=>acc+curr)
    .take(10)
    .foreach(println)


}
