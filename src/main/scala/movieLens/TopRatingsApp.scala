package movieLens

import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac039 on 2017/5/15.
  */
object TopRatingsApp extends App{
  val conf = new SparkConf().setAppName("TopRatings")
    .setMaster("local[*]")
  val sc = new SparkContext(conf)

  val ratings=sc.textFile("/Users/mac039/Downloads/ml-20m/ratings.csv")
    .map(str=>str.split(","))
    .filter(strs=>strs(1)!="movieId")
    .map(strs=>{

      (strs(1).toInt,strs(2).toDouble)

    })

 //val totalRatingByMovieId=ratings.reduceByKey((acc,curr)=>acc+curr)
  //totalRatingByMovieId.takeSample(false,5).foreach(println) //計算總分 取後不放回 跑五個

  val averageRatingByMovieId=ratings.mapValues(v=> v->1)
    .reduceByKey((acc,curr)=>{
      (acc._1+curr._1)->(acc._2+curr._2)

    }).mapValues(kv=>kv._1/kv._2.toDouble)

  averageRatingByMovieId.takeSample(false,5).foreach(println)

  val movies=sc.textFile("/Users/mac039/Downloads/ml-20m/movies.csv")
    .map(str=>str.split(","))
   .filter(strs=>strs(0)!="movieId")
  .map(strs=>strs(0).toInt->strs(1))

  val joined=averageRatingByMovieId.join(movies)
  //joined.takeSample(false,5).foreach(println)
joined.map(v=>v._1+","+v._2._1).saveAsTextFile("result")




  //ratings.take(5).foreach(println)


}
